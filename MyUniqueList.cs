﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestPart2Generics
{
    class MyUniqueList<T>
    {
        List<T> list = new List<T>();

        public MyUniqueList()
        {

        }
        public bool Add(T item)
        {
            if (list.Contains(item))
            {
                list.Add(item);
                return true;
            }

            return false;
        }

        public bool Remove(T item)
        {
            if (list.Contains(item))
            {
                list.Remove(item);
                return true;
            }

            return false;
        }

        public T Peek(int index)
        {
            return list[index];
        }

        public T this[int index]
        {
            get
            {
                return this.list[index];
            }
            set
            {
/*                i just removed that part because if (list[index] == value) will be true the last line will just ovverride it to the same value.
                if (list[index] == value)
                    return;*/
                if (list.Contains(value))
                    return;
                list[index] = value;
            }
        }
    }

}
